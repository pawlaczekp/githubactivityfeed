//
//  MyAssertionHandler.swift
//  GitHubActivityFeedTests
//
//  Created by ppawlaczek on 03/05/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//
//
//import Foundation
//import Nimble
//import Quick
//
//class FixAssertionHandler : AssertionHandler {
//    func assert(_ assertion: Bool, message: FailureMessage, location: SourceLocation) {
//        if (!assertion) {
//            print("Expectation failed: \(message.stringValue)")
//        }
//    }
//}
