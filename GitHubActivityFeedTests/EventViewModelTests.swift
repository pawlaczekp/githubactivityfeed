//
//  EventsViewModelTests.swift
//  GitHubActivityFeedTests
//	
//  Created by ppawlaczek on 03/05/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import SwiftyJSONModel
@testable import SwiftyJSON
@testable import GitHubActivityFeed
//NimbleAssertionHandler = FixAssertionHandler()
class EventViewModelTests: QuickSpec {
    
    override func spec() {
        var subject: EventViewModel!
        context("When EventViewModel is created") {
            beforeEach {
               let path = Bundle(for: type(of: self)).path(forResource:"event",ofType: "json")
               let stringJSON = try! String(contentsOfFile: path!)
               let json = JSON.init(parseJSON:stringJSON)
                
                subject = EventViewModel()
                subject.event = try? Event(json: json)
            }
            it("it ", closure: {
                expect(subject.event?.id).to(equal("12227701205"))
                expect(subject.eventDateFormatted).to(equal("2020-05-03  18:19:06+02:00"))
                expect(subject.event?.isPublic).to(beTrue())
            })
        }
    }
    
    
}
