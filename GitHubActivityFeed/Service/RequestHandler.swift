//
//  RequestHandler.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 30/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation
import SwiftyJSONModel
import SwiftyJSON

class RequestHandler {
    
   // let reachability = Reachability()!
    
    func networkResult(completion: @escaping ((Result<[Event], ErrorResult>) -> Void)) -> ((Result<Data, ErrorResult>) -> Void) {
            
            return { dataResult in
                
                DispatchQueue.global(qos: .background).async(execute: {
                    switch dataResult {
                    case .success(let data) :
                        self.parseData(data: data, completion: { result in
                            //completion
                            completion(result)
                        })
                        break
                    case .failure(let error) :
                        print("Network error \(error)")
                        completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                        break
                    }
                })
                
            }
    }
    
    
    func parseData(data: Data, completion:(Result<[Event], ErrorResult>) -> Void) {
        
        do {
            var finalResult : [Event] = []
            let result = try JSON(data: data)
            for item in result {
                if let event = try? Event(json: item.1) {
                        finalResult.append(event)
                }
            }
           
            completion(.success(finalResult))
        } catch {
            completion(.failure(.parser(string: "Error while parsing json data")))
        }
        
    }
    
}
