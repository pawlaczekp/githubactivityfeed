//
//  ErrorResult.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 29/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case unknown(string: String)
}
