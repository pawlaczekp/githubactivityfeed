//
//  RequestFactory.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 29/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation

final class RequestFactory {
    static let baseUrl = "https://api.github.com/"
    
    enum Method: String {
        case GET
        case POST
    }
    
    enum Endpoint: String {
        case events
        case feeds
        
        func requestGet()->URLRequest? {
            return prepareRequest(method: RequestFactory.Method.GET)
        }
        
        func prepareRequest(method: Method) -> URLRequest? {
            
            var endpointUrl: String
            switch self {
                case .events:
                    endpointUrl = baseUrl + RequestFactory.Endpoint.events.rawValue
                case .feeds:
                    endpointUrl = baseUrl + RequestFactory.Endpoint.feeds.rawValue
            }
            guard let url = URL(string: endpointUrl) else {
                return nil
            }
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            return request
        }
    }
    
    
    static func request(method: Method, url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        return request
    }
    
}
