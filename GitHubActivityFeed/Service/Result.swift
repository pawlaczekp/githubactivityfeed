//
//  Result.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 30/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
