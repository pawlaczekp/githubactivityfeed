//
//  EventsService.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 29/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation


final class EventsService: RequestHandler {
    
    static let shared = EventsService()
    
    var task : URLSessionTask?
    
    func fetchEvents(_ completion: @escaping ((Result<[Event], ErrorResult>) -> Void)) {
        
        // cancel previous request if already in progress
        self.cancelFetching()
        
        task = RequestService().loadData(endpointType: RequestFactory.Endpoint.events, completion: self.networkResult(completion: completion) )
    }
    
    func cancelFetching() {
        
        if let task = task {
            task.cancel()
        }
        task = nil
    }
}

