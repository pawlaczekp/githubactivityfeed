//
//  EventsDataSource.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 30/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation
import UIKit

class GenericDataSource<T> : NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}

class EventsDataSource : GenericDataSource<Event>, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
        cell.event = self.data.value[indexPath.row]
        
        return cell
    }
    
    
    
}
