//
//  EventViewModel.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 29/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation

struct EventsViewModel {
    weak var dataSource: GenericDataSource<Event>?
    weak var service: EventsService?
    
    var onErrorHandling: ((ErrorResult?) -> Void)?
    
    init(service: EventsService = EventsService.shared, dataSource: GenericDataSource<Event> ) {
        self.dataSource = dataSource
        self.service = service
    }
    
    func fetchEvents() {
        guard let service = service else {
            onErrorHandling?(ErrorResult.unknown(string: "Missing service"))
            return
        }
        
        service.fetchEvents { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let events) :
                    self.dataSource?.data.value = events
                    break
                case .failure(let error) :
                    self.onErrorHandling?(error)
                    break
                }
            }
        }
    }
    
}
