//
//  EventViewModel.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 30/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation


class EventViewModel {
    let dateFormatPattern = "yyyy-MM-dd  HH:mm:ssZZZZZ"
    let formatter = DateFormatter()
    var eventDateFormatted: String?
    
    var event: Event? {
           didSet {
               guard let event = event else {
                   return
               }
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormatPattern
            eventDateFormatted = formatter.string(from: event.created)
           }
       }
    
}
