//
//  DetailsViewController.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 30/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import UIKit


class DetailsViewController: UIViewController {
    
    @IBOutlet weak var eventId: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var organisation: UILabel!
    
    lazy var viewModel: EventViewModel? = {
        return EventViewModel()
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillUI()
    }
    
    
    fileprivate func fillUI() {
        if !isViewLoaded {
            return
        }
        
        guard let viewModel = viewModel else {
            return
        }
        
        
        eventId?.text = viewModel.event?.id
        eventDate?.text = viewModel.eventDateFormatted
        eventType?.text = viewModel.event?.type
        author?.text = viewModel.event?.actor?.login
        organisation?.text = viewModel.event?.organisation?.login
    }
    
    
    
    
}
