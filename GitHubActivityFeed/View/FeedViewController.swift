//
//  FeedViewController.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 29/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var tableView : UITableView!
    
    let dataSource = EventsDataSource()
    
    lazy var viewModel: EventsViewModel = {
        let viewModel = EventsViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Github events lists"
        tableView.delegate = self
        self.tableView.dataSource = self.dataSource
        self.dataSource.data.addAndNotify(observer: self) { [weak self] _ in
            self?.tableView.reloadData()
        }
        self.viewModel.fetchEvents()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailsViewController = storyboard?.instantiateViewController(identifier: "DetailsViewController") as? DetailsViewController {
            navigationController?.pushViewController(detailsViewController, animated: true)
            detailsViewController.viewModel?.event = self.dataSource.data.value[indexPath.row]
        }
    }
}
