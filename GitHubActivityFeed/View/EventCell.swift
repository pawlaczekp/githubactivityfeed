//
//  EventCell.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 30/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {
    
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var  type: UILabel!
    @IBOutlet weak var  created: UILabel!
    
    let formatter = DateFormatter()
    
    var event: Event? {
        didSet {
            guard let event = event else {
                return
            }
            author?.text = event.actor?.login
            type?.text = event.type
            created?.text = formatter.string(from: event.created)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        formatter.dateFormat = "yyyy-MM-dd  HH:mm:ssZZZZZ"
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
