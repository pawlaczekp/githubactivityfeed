//
//  Organisation.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 29/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation
import SwiftyJSONModel


struct Organisation {
    var id: Int
    var login: String
    var gravatarId: String
    var avatarUrl: String
    var userUrl: String
}

extension Organisation: JSONModelType {
    enum PropertyKey: String {
        case id, login, gravatar_id, avatar_url, url
    }
    
    init(object: JSONObject<Self.PropertyKey>) throws {
        id = try object.value(for: .id)
        login = try object.value(for: .login)
        gravatarId = try object.value(for: .gravatar_id)
        avatarUrl = try object.value(for: .avatar_url)
        userUrl = try object.value(for: .url)
    }
    
    var dictValue: [Organisation.PropertyKey : JSONRepresentable?] {
        return [
            .id: id,
            .login: login,
            .gravatar_id: gravatarId,
            .avatar_url: avatarUrl,
            .url: userUrl,
        ]
    }

    
}

