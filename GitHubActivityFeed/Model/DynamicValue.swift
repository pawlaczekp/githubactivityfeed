//
//  DynamicValue.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 30/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation

class DynamicValue<T> {
typealias CompletionHandler = ((T) -> Void)
    var value : T {
        didSet {
            self.notify()
        }
    }

    private var observers = [String: CompletionHandler]()
    private var observer : CompletionHandler?
    
    
    init(_ value: T) {
        self.value = value
    }

    public func addObserver(_ observer: NSObject, completionHandler: @escaping CompletionHandler) {
        observers[observer.description] = completionHandler
    }

    public func addAndNotify(observer: NSObject, completionHandler: @escaping CompletionHandler) {
        self.addObserver(observer, completionHandler: completionHandler)
        self.notify()
    }

    private func notifyOneObserver() {
        observer?(value)
    }
    
    private func notify() {
        observers.forEach({ $0.value(value) })
    }

    deinit {
        observers.removeAll()
    }
}
