//
//  Event.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 29/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation
import SwiftyJSONModel

struct Event {
    var id: String
    var type: String
    var isPublic: Bool
    var created: Date
    var repository: Repository?
    var actor: Actor?
    var organisation: Organisation?
    
    let dateTransformer: DateTransformer = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
}

extension Event: JSONModelType {
    enum PropertyKey: String {
        case id,type,`public`,created_at, repo, actor, org
        
    }
    
    init(object: JSONObject<PropertyKey>) throws {
        id = try object.value(for: .id)
        type = try object.value(for: .type)
        isPublic = try object.value(for: .public)
        created = try object.value(for: .created_at, with: dateTransformer )
        repository = try object.value(for: .repo)
        actor = try object.value(for: .actor)
        organisation = try object.value(for: .org)
    }
    
    var dictValue: [PropertyKey : JSONRepresentable?] {
    return [
        .id: id,
        .type: type,
        .public: isPublic,
        .created_at: dateTransformer.string(form: created),
        .repo: repository,
        .actor: actor,
        .org: organisation,
        ]
    }
}

