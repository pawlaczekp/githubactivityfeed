//
//  Repository.swift
//  GitHubActivityFeed
//
//  Created by ppawlaczek on 29/04/2020.
//  Copyright © 2020 ppawlaczek. All rights reserved.
//

import Foundation
import SwiftyJSONModel

struct Repository {
    var id: Int
    var name: String
    var url: String
}

extension Repository: JSONModelType {
    enum PropertyKey: String {
        case id, name, url
    }
    
    init(object: JSONObject<Self.PropertyKey>) throws {
        id = try object.value(for: .id)
        name = try object.value(for: .name)
        url = try object.value(for: .url)
    }
    
    var dictValue: [PropertyKey : JSONRepresentable?] {
    return [
        .id: id,
        .name: name,
        .url: url,
        ]
    }
    
}
